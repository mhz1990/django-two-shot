from receipts.models import Receipt, ExpenseCategory, Account
from django.forms import ModelForm

# from django.contrib.auth.models import User


class CreateForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
            # "purchaser",
        ]


class CategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
            # "owner",
        ]


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
